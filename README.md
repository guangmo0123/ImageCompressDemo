# ImageCompressDemo

#### 介绍
`ImageUtils.java`为压缩图片的工具类；
`CharacterCount.java`为字符统计的工具类，如统计代码字数或小说字数等等。

#### 使用说明

1. `ImageUtils`和`CharacterCount`都包含`main`方法，所以都可以直接运行；
2. `ImageUtils`可以将图片按设定的快高最大值以及图片大小来进行压缩。

    2.1 如原图1000x600，输入宽高最大值为800，则压缩为800x480；
    
    2.2 如原图600x1000，输入宽高最大值为800，则压缩为480x800；
    
    2.3 还可以指定压缩后图片的大小（单位KB），若输入0，则不控制压缩后图片的大小；
    
    2.4 新增给图片添加文字水印和图片水印，并增加`config.properties`配置文件
    
3. `CharacterCount`可以统计字符个数，其中可以区分中文、英文、标点符号等等每种字符的个数，当初写这个工具时，最开始的目的是为了统计一部小说的字数[捂脸]；