import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class ImageTest {

    public static void main(String[] args) throws Exception {
        String imgFilePath = "G:\\压缩图片\\待压缩图片\\111.jpg";
        String waterImagePath = "G:\\压缩图片\\logo\\logo-big.png";
        String waterText = "NW413-A065";
        Color color = new Color(255, 0, 0, 128);
        Font font = new Font("微软雅黑", Font.BOLD, 48);
        File outDirFile = new File("G:\\压缩图片\\压缩后图片");

        BufferedImage srcImage = ImageIO.read(new File(imgFilePath));

        BufferedImage iconWaterImage = addImageWaterMark(srcImage, waterImagePath);
        ImageIO.write(iconWaterImage, "jpg", new File(outDirFile, "iconWaterImage.jpg"));

        BufferedImage textWaterImage = addTextWaterMark(iconWaterImage, waterText, color, font);
        ImageIO.write(textWaterImage, "jpg", new File(outDirFile, "textWaterImage.jpg"));


        addImageWaterMark2(srcImage, waterImagePath);
        ImageIO.write(srcImage, "jpg", new File(outDirFile, "iconWaterImage2.jpg"));

        addTextWaterMark2(srcImage, waterText, color, font);
        ImageIO.write(textWaterImage, "jpg", new File(outDirFile, "textWaterImage2.jpg"));
    }

    /**
     * 为图片添加图片水印
     */
    public static BufferedImage addImageWaterMark(BufferedImage srcImg, String waterImagePath) {
        int width = srcImg.getWidth(null);//水印宽度
        int height = srcImg.getHeight(null);//水印高
        BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = bi.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g.drawImage(srcImg.getScaledInstance(width, height, Image.SCALE_SMOOTH), 0, 0, null);
        ImageIcon imgIcon = new ImageIcon(waterImagePath);
        Image con = imgIcon.getImage();
        float clarity = 1f;//透明度
        g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, clarity));
        g.drawImage(con, 15, 15, null);//水印的位置
        g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER));
        g.dispose();
        return bi;
    }

    /**
     * 为图片添加图片水印
     */
    public static void addImageWaterMark2(BufferedImage srcImg, String waterImagePath) {
        Graphics2D g = srcImg.createGraphics();
        g.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        ImageIcon imgIcon = new ImageIcon(waterImagePath);
        Image con = imgIcon.getImage();
        float clarity = 1f;//透明度
        g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, clarity));
        g.drawImage(con, 15, 15, null);//水印的位置
        g.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER));
        g.dispose();
    }

    /**
     * 为图片添加文字水印
     */
    public static BufferedImage addTextWaterMark(BufferedImage srcImg, String waterText, final Color color, final Font font) {
        int srcImgWidth = srcImg.getWidth(null);
        int srcImgHeight = srcImg.getHeight(null);
        BufferedImage buffImg = new BufferedImage(srcImgWidth, srcImgHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = buffImg.createGraphics();
        g.drawImage(srcImg, 0, 0, srcImgWidth, srcImgHeight, null);
        g.setColor(color);
        g.setFont(font);
        //设置水印的坐标
        int x = srcImgWidth - (g.getFontMetrics(g.getFont()).charsWidth(
                waterText.toCharArray(), 0, waterText.length()) + 15);
        int y = srcImgHeight - 15;
        g.drawString(waterText, x, y);  //加水印
        g.dispose();
        return buffImg;
    }

    /**
     * 为图片添加文字水印
     */
    public static void addTextWaterMark2(BufferedImage srcImg, String waterText, final Color color, final Font font) {
        int srcImgWidth = srcImg.getWidth(null);
        int srcImgHeight = srcImg.getHeight(null);
        Graphics2D g = srcImg.createGraphics();
        g.setColor(color);
        g.setFont(font);
        //设置水印的坐标
        int x = srcImgWidth - (g.getFontMetrics(g.getFont()).charsWidth(
                waterText.toCharArray(), 0, waterText.length()) + 15);
        int y = srcImgHeight - 15;
        g.drawString(waterText, x, y);  //加水印
        g.dispose();
    }
}
